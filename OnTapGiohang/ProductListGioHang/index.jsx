import React, { Component } from "react";
import ProductItemGioHang from "../ProductItemGioHang";
import { connect } from "react-redux";

class ProductListGiohang extends Component {
  renderSanPham = () => {
    let { productList, themSanPham } = this.props;
    return productList.map((sp, index) => {
      return (
        <ProductItemGioHang
          sanPham={sp}
          key={index}
          themSanPham={themSanPham}
        ></ProductItemGioHang>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">{this.renderSanPham()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    productList: state.gioHangReducer.productList,
  };
};

export default connect(mapStateToProps)(ProductListGiohang);

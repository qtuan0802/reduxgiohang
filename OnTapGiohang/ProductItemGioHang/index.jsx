import React, { Component } from "react";
import { connect } from "react-redux";

class ProductItemGioHang extends Component {
  render() {
    let { sanPham, themSanPham } = this.props;
    return (
      <div className="col-3">
        <div className="card">
          <img src={sanPham.img} alt="hinhAnh" width={200} height={250}></img>
          <p>{sanPham.name}</p>
          <p>{sanPham.price.toLocaleString()}</p>
          <button
            className="btn btn-danger"
            onClick={() => themSanPham(sanPham)}
          >
            Thêm Sản Phẩm
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    themSanPham: (sanPham) => {
      let spGH = {
        id: sanPham.id,
        name: sanPham.name,
        img: sanPham.img,
        price: sanPham.price,
        amount: 1,
      };
      const actions = {
        type: "THEM_GIO_HANG",
        spGH: spGH,
      };
      console.log(actions);
      dispatch(actions);
    },
  };
};
export default connect(null, mapDispatchToProps)(ProductItemGioHang);

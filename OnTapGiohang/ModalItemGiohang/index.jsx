import React, { Component } from "react";
import { connect } from "react-redux";

class ModalItemGiohang extends Component {
  render() {
    let { item, xoaSanPhamIndex, thayDoiAmount } = this.props;
    return (
      <tr>
        <td>{item.id}</td>
        <td>{item.name}</td>
        <td>
          <img src={item.img} alt="" width="50px" />
        </td>
        <td>{item.price.toLocaleString()} </td>

        <td>
          <button
            className="btn btn-primary"
            onClick={() => thayDoiAmount(item, false)}
          >
            -
          </button>
          {item.amount}
          <button
            className="btn btn-primary"
            onClick={() => thayDoiAmount(item, true)}
          >
            +
          </button>
        </td>

        <td>{(item.price * item.amount).toLocaleString()}</td>

        <button
          className="btn btn-danger mt-2"
          onClick={() => xoaSanPhamIndex(item)}
        >
          Xóa
        </button>
      </tr>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    xoaSanPhamIndex: (spGH) => {
      let spUpdate = {
        id: spGH.id,
        name: spGH.name,
        img: spGH.img,
        price: spGH.price,
        amount: 1,
      };
      const actions = {
        type: "XOA_SAN_PHAM",
        spUpdate: spUpdate,
      };
      console.log(actions);
      dispatch(actions);
    },
  };
};
export default connect(null, mapDispatchToProps)(ModalItemGiohang);
// export default ModalItemGiohang;

import React, { Component } from "react";
import HeaderGiohang from "../HeaderGiohang";
import ModalGiohang from "../ModalGiohang";
import ProductListGiohang from "../ProductListGioHang";

class HomeGioHang extends Component {
  // mangSanPham = [
  //   {
  //     id: "sp_1",
  //     name: "iphoneX",
  //     price: 30000000,
  //     screen: "screen_1",
  //     backCamera: "backCamera_1",
  //     frontCamera: "frontCamera_1",
  //     img:
  //       "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg",
  //     desc:
  //       "iPhone X features a new all-screen design. Face ID, which makes your face your password",
  //   },
  //   {
  //     id: "sp_2",
  //     name: "Note 7",
  //     price: 20000000,
  //     screen: "screen_2",
  //     backCamera: "backCamera_2",
  //     frontCamera: "frontCamera_2",
  //     img:
  //       "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png",
  //     desc:
  //       "The Galaxy Note7 comes with a perfectly symmetrical design for good reason",
  //   },
  //   {
  //     id: "sp_3",
  //     name: "Vivo",
  //     price: 10000000,
  //     screen: "screen_3",
  //     backCamera: "backCamera_3",
  //     frontCamera: "frontCamera_3",
  //     img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
  //     desc:
  //       "A young global smartphone brand focusing on introducing perfect sound quality",
  //   },
  //   {
  //     id: "sp_4",
  //     name: "Blacberry",
  //     price: 15000000,
  //     screen: "screen_4",
  //     backCamera: "backCamera_4",
  //     frontCamera: "frontCamera_4",
  //     img:
  //       "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
  //     desc:
  //       "BlackBerry is a line of smartphones, tablets, and services originally designed",
  //   },
  // ];
  state = {
    gioHang: [],
  };

  // lấy dữ liệu tại component Home
  themSanPham = (sanPham) => {
    // từ sản phẩm được chọn , tạo ra sản phẩm giỏ hàng
    let sanPhamGH = {
      id: sanPham.id,
      name: sanPham.name,
      img: sanPham.img,
      price: sanPham.price,
      amount: 1,
    };
    //kiểm tra sản phẩm chọn có trong giỏ hàng
    var gioHangCapNhat = [...this.state.gioHang];
    let index = gioHangCapNhat.findIndex((sp) => {
      return sp.id === sanPhamGH.id;
    });
    if (index !== -1) {
      // sản phẩm được click đã có trong this.state.giohang
      gioHangCapNhat[index].amount += 1;
    } else {
      // sản phẩm được click chưa có trong this.state.giohang
      gioHangCapNhat.push(sanPhamGH);
    }
    this.setState({
      gioHang: gioHangCapNhat,
    });
  };

  xoaSanPham = (sanPham) => {
    console.log(sanPham);
    var gioHangXoa = [...this.state.gioHang];
    gioHangXoa = gioHangXoa.filter((sp) => {
      return sp.id !== sanPham.id;
    });
    // let index = gioHangXoa.findIndex((item) => {
    //   return item.id === sanPham.id;
    // });
    // if (index !== -1) {
    //   gioHangXoa.splice(index, 1);
    // }
    this.setState({
      gioHang: gioHangXoa,
    });
  };

  thayDoiAmount = (sanPham, status) => {
    let changeAmount = [...this.state.gioHang];
    let index = changeAmount.findIndex((sp) => {
      return sp.id === sanPham.id;
    });
    if (index !== -1) {
      if (status === true) {
        changeAmount[index].amount += 1;
      } else if (status === false) {
        if (changeAmount[index].amount > 1) {
          changeAmount[index].amount -= 1;
        }
      }
    }
    this.setState({
      gioHang: changeAmount,
    });
  };

  render() {
    let tongSoLuong = this.state.gioHang.reduce((tsl, sanPham, index) => {
      return (tsl += sanPham.amount);
    }, 0);
    return (
      <div>
        <HeaderGiohang tongSoLuong={tongSoLuong}></HeaderGiohang>
        <ProductListGiohang
          // mangSanPham={this.mangSanPham}
          themSanPham={this.themSanPham}
        ></ProductListGiohang>
        <ModalGiohang
          gioHang={this.state.gioHang}
          // xoaSanPham={this.xoaSanPham}
          thayDoiAmount={this.thayDoiAmount}
        ></ModalGiohang>
      </div>
    );
  }
}

export default HomeGioHang;

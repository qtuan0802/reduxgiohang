import React, { Component } from "react";
import ModalItemGiohang from "../ModalItemGiohang";
// import ModalItemGiohang from "../ModalItemGiohang";
import { connect } from "react-redux";

class ModalGiohang extends Component {
  renderGioHang = () => {
    let { cartList, xoaSanPham, thayDoiAmount } = this.props;
    return cartList.map((item, index) => {
      return (
        <ModalItemGiohang
          item={item}
          key={index}
          xoaSanPham={xoaSanPham}
          thayDoiAmount={thayDoiAmount}
        ></ModalItemGiohang>
      );
    });
  };

  render() {
    return (
      <div>
        {/* Modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div
            className="modal-dialog"
            role="document"
            style={{
              width: "1000px",
              maxWidth: "1000px",
            }}
          >
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Giỏ Hàng
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table className="table">
                  <thead>
                    <tr>
                      <td>Mã Sản Phẩm</td>
                      <td>Tên Sản Phẩm</td>
                      <td>Hình ảnh</td>
                      <td>Đơn Giá</td>
                      <td>Số Lượng</td>
                      <td>Thành Tiền</td>
                      <td>Hành Động</td>
                    </tr>
                  </thead>
                  <tbody>{this.renderGioHang()}</tbody>
                  <tfoot>
                    <tr>
                      <td colspan="5" className="text-right">
                        Tổng Tiền
                      </td>
                      <td>
                        {this.props.gioHang
                          .reduce((tt, sp, index) => {
                            return (tt += sp.price * sp.amount);
                          }, 0)
                          .toLocaleString()}
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>
        ;
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    cartList: state.gioHangReducer.cartList,
  };
};
export default connect(mapStateToProps)(ModalGiohang);
